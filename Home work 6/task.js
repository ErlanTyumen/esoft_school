class GameField {
  state = [
    [null, null, null],
    [null, null, null],
    [null, null, null]
  ];
  mode = true;
  isOverGame = false;
  getGameStateStatus() {
    return ('' + this.state[0][0] + this.state[0][1] + this.state[0][2] + '\n' + this.state[1][0] + this.state[1][1] +this.state[1][2] + '\n' + this.state[2][0] + this.state[2][1] + this.state[2][2])
  }
  setMode() {
    this.mode = !this.mode;
  }
  stateCellValue(x, y) {
    if (this.state[x][y] == null) {
      if (this.mode == true)
        this.state[x][y] = 'x';
      else
        this.state[x][y] = 'o';
      this.setMode();
      this.checkMap();
      console.log(this.getGameStateStatus());
    }
    else {
    console.log('Ячейка занята');
    console.log(this.getGameStateStatus());
  }
  }
  checkMap() {
    if (this.state[0][0] == 'x' && this.state[0][1] == 'x' && this.state[0][2] == 'x' ||
      this.state[1][0] == 'x' && this.state[1][1] == 'x' && this.state[1][2] == 'x' ||
      this.state[2][0] == 'x' && this.state[2][1] == 'x' && this.state[2][2] == 'x' ||
      this.state[0][0] == 'x' && this.state[1][0] == 'x' && this.state[2][0] == 'x' ||
      this.state[0][1] == 'x' && this.state[1][1] == 'x' && this.state[2][1] == 'x' ||
      this.state[0][2] == 'x' && this.state[1][2] == 'x' && this.state[2][2] == 'x' ||
      this.state[0][0] == 'x' && this.state[1][1] == 'x' && this.state[2][2] == 'x' ||
      this.state[0][2] == 'x' && this.state[1][1] == 'x' && this.state[2][0] == 'x') {
      this.isOverGame = true;
      console.log('Крестики победили');
    }
    else if (this.state[0][0] == 'o' && this.state[0][1] == 'o' && this.state[0][2] == 'o' ||
      this.state[1][0] == 'o' && this.state[1][1] == 'o' && this.state[1][2] == 'o' ||
      this.state[2][0] == 'o' && this.state[2][1] == 'o' && this.state[2][2] == 'o' ||
      this.state[0][0] == 'o' && this.state[1][0] == 'o' && this.state[2][0] == 'o' ||
      this.state[0][1] == 'o' && this.state[1][1] == 'o' && this.state[2][1] == 'o' ||
      this.state[0][2] == 'o' && this.state[1][2] == 'o' && this.state[2][2] == 'o' ||
      this.state[0][0] == 'o' && this.state[1][1] == 'o' && this.state[2][2] == 'o' ||
      this.state[0][2] == 'o' && this.state[1][1] == 'o' && this.state[2][0] == 'o') {
      this.isOverGame = true;
      console.log('Нолики победили');
    }
    else if (this.state[0][0] == null && this.state[0][1] == null && this.state[0][2] == null &&
      this.state[1][0] == null && this.state[1][1] == null && this.state[1][2] == null &&
      this.state[2][0] == null && this.state[2][1] == null && this.state[2][2] == null) {
      this.isOverGame = true;
      console.log('Ничья');
    }
    else
      return 'Игра неокончена';
  }
}

const gameField  = new GameField();
while(!gameField.isOverGame) {
  let inputX = prompt();
  let inputY = prompt();
  gameField.stateCellValue(inputX, inputY)
}

console.log(gameField .getGameStateStatus());