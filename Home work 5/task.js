function checkMap(items) {
    if (items[0][0] == 'x' && items[0][1] == 'x' && items[0][2] == 'x' ||
      items[1][0] == 'x' && items[1][1] == 'x' && items[1][2] == 'x' ||
      items[2][0] == 'x' && items[2][1] == 'x' && items[2][2] == 'x' ||
      items[0][0] == 'x' && items[1][0] == 'x' && items[2][0] == 'x' ||
      items[0][1] == 'x' && items[1][1] == 'x' && items[2][1] == 'x' ||
      items[0][2] == 'x' && items[1][2] == 'x' && items[2][2] == 'x' ||
      items[0][0] == 'x' && items[1][1] == 'x' && items[2][2] == 'x' ||
      items[0][2] == 'x' && items[1][1] == 'x' && items[2][0] == 'x')
      return 'Крестики победили';
    else if (items[0][0] == 'o' && items[0][1] == 'o' && items[0][2] == 'o' ||
      items[1][0] == 'o' && items[1][1] == 'o' && items[1][2] == 'o' ||
      items[2][0] == 'o' && items[2][1] == 'o' && items[2][2] == 'o' ||
      items[0][0] == 'o' && items[1][0] == 'o' && items[2][0] == 'o' ||
      items[0][1] == 'o' && items[1][1] == 'o' && items[2][1] == 'o' ||
      items[0][2] == 'o' && items[1][2] == 'o' && items[2][2] == 'o' ||
      items[0][0] == 'o' && items[1][1] == 'o' && items[2][2] == 'o' ||
      items[0][2] == 'o' && items[1][1] == 'o' && items[2][0] == 'o')
      return 'Нолики победили';
    else if (items[0][0] == null && items[0][1] == null && items[0][2] == null &&
      items[1][0] == null && items[1][1] == null && items[1][2] == null &&
      items[2][0] == null && items[2][1] == null && items[2][2] == null)
      return 'Ничья';
    else
      return 'Игра неокончена';
  }

  const gameField = [
    ['x', 'o', null],
    ['x', null, 'o'],
    [null, 'o', 'o']
  ];

console.log(checkMap(gameField))